//
//  LooperGui.cpp
//  sdaLooper
//
//  Created by tj3-mitchell on 21/01/2013.
//
//

#include "LooperGui.h"

LooperGui::LooperGui(Looper& looper_) : looper (looper_) 
{
    playButton.setButtonText ("Play");
    playButton.setConnectedEdges(Button::ConnectedOnLeft | Button::ConnectedOnRight);
    playButton.setColour(TextButton::buttonColourId, Colours::grey);
    playButton.setColour(TextButton::buttonOnColourId, Colours::lightgrey);
    addAndMakeVisible (&playButton);
    playButton.addListener (this);
    
    recordButton.setButtonText ("Record");
    recordButton.setConnectedEdges(Button::ConnectedOnLeft | Button::ConnectedOnRight);
    recordButton.setColour(TextButton::buttonColourId, Colours::darkred);
    recordButton.setColour(TextButton::buttonOnColourId, Colours::red);
    addAndMakeVisible (&recordButton);
    recordButton.addListener (this);
    

    
}


void LooperGui::buttonClicked (Button* button)
{
    if (button == &playButton)
    {
        looper.setPlayState (!looper.getPlayState());
        playButton.setToggleState (looper.getPlayState(), dontSendNotification);
        if (looper.getPlayState())
            playButton.setButtonText ("Stop");
        else
            playButton.setButtonText ("Play");
    }
    else if (button == &recordButton)
    {
        looper.setRecordState (!looper.getRecordState());
        recordButton.setToggleState (looper.getRecordState(), dontSendNotification);

    }
}

void LooperGui::resized()
{
    playButton.setBounds (0, 0, getWidth()/2, getHeight()/2);
    recordButton.setBounds (playButton.getBounds().translated(getWidth()/2, 0));
    
   
}

//LooperGui2::LooperGui2(Looper2& looper2_) : looper2 (looper2_)
//{
//    playButton2.setButtonText ("Play2");
//    playButton2.setConnectedEdges(Button::ConnectedOnLeft | Button::ConnectedOnRight);
//    playButton2.setColour(TextButton::buttonColourId, Colours::grey);playButton2.setColour(TextButton::buttonOnColourId,Colours::lightgrey);
//    addAndMakeVisible (&playButton2);playButton2.addListener (this);
//    
//    recordButton2.setButtonText ("Record2");
//    recordButton2.setConnectedEdges(Button::ConnectedOnLeft | Button::ConnectedOnRight);
//    recordButton2.setColour(TextButton::buttonColourId, Colours::darkred);
//    recordButton2.setColour(TextButton::buttonOnColourId, Colours::red);
//    addAndMakeVisible (&recordButton2);
//    recordButton2.addListener (this);
//    
//    
//    
//}
//void LooperGui2::buttonClicked (Button* button)
//{
//    if (button == &playButton2)
//    {
//        looper2.setPlayState (!looper2.getPlayState());
//        playButton2.setToggleState (looper2.getPlayState(), dontSendNotification);
//        if (looper2.getPlayState())
//            playButton2.setButtonText ("Stop");
//        else
//            playButton2.setButtonText ("Play");
//    }
//    else if (button == &recordButton2)
//    {
//        looper2.setRecordState (!looper2.getRecordState());
//        recordButton2.setToggleState (looper2.getRecordState(), dontSendNotification);
//        
//    }
//}
//
//void LooperGui2::resized()
//{
//    playButton2.setBounds (0, 0, getWidth()/2, getHeight()/2);
//    recordButton2.setBounds (playButton2.getBounds().translated(getWidth()/2, 0));
//    
//    
//}
